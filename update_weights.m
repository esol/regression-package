function net2 = update_weights(net,grads)
% updates network weights
% net2 is the updated one
% net is the old one
% grads is the output of stochastic_gradients
net2=net;

% hope this speeds things up
%net.layer{(net.N)-1}.W=[];

for i=1:net.N-2

net2.layer{i}.W = net.layer{i}.W+grads.W{i};
%grads.W{i}
%grads.F{i+1}
net2.layer{i}.F = net.layer{i}.F+grads.F{i+1};
net2.layer{i}.B = net.layer{i}.B+grads.B{i+1};
net2.layer{i}.b = net.layer{i}.b+grads.b{i};
end
% update weights of output layer
%net2.layer{net.N-1}.W = net.layer{net.N-1}.W + grads.W{net.N-1};
net2.layer{net.N-1}.W = net.layer{net.N-1}.W + repmat(grads.W{net.N-1},size(net.layer{net.N-1}.W,1),1);
% update bias term in output layer
net2.layer{net.N-1}.b = net2.layer{net.N-1}.b + grads.b{net.N-1};
%grads

end
