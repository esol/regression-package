function grads = stochastic_gradients(deltas,aux,alpha, network)
% function for calculating stochastic gradients for network parameters
% deltas are given by calculate_deltas
% aux is given by evaluator
% alpha is the learning parameter
% network is the network
sizes=network.sizes;
N = network.N;

%aux
%deltas

% this might speed things up
grads.W{N-1}=[];
grads.F{N-1}=[];
grads.B{N-1}=[];
grads.b{N-1}=[];
% go through layers
for i=1:N-1
s = aux{i};
s2= deltas{i+1};
%s2= aux{i+1};
%size(s2)
%sizes
%i

grads.W{i}=zeros(sizes(i+1),sizes(i)*2);
size(s2,1);
    for j=1:size(s2,1)/2

	    
    for jp =1:sizes(i)*2
%        k=rem(jp,sizes(i));
%	tmp = s(jp,:);
%	tmp2= s2(j,:);
if(i<N-1)
tmp = s2(j,:) + s2(j+sizes(i+1),:);
else
	tmp=s2(j,:);
end

tmp2 = s(jp,:);
% multiplying      
        tmp3=-alpha*mean(tmp.*tmp2);
        grads.W{i}(j,jp) = grads.W{i}(j,jp) +tmp3;
    end	    
    end
    
% bias updates    
grads.b{i}=-alpha*mean(s2,2); 


% updates to forward weights
grads.F{i} = zeros(sizes(i),sizes(i));
grads.F{i};
s2= deltas{i};
    for j=1:sizes(i)
    for k=1:sizes(i)
%	    j,k
    tmp = s2(k,:).*s(j,:);
    grads.F{i}(j,k) = grads.F{i}(j,k)-alpha*mean(tmp);
    end
    end	    

% updates to backward weights
grads.B{i} = zeros(sizes(i),sizes(i));
    for j=sizes(i)+1:2*sizes(i)
    for k=sizes(i)+1:2*sizes(i)
    tmp = s2(k,:).*s(j,:);
    grads.B{i}(j-sizes(i),k-sizes(i)) = grads.B{i}(j-sizes(i),k-sizes(i))-alpha*mean(tmp);
    end
    end	    
end	

% output weights
a1=deltas{N-1};
a2=aux{N-1};
%a2=aux{N};
a3= a1.*a2;
%a3=a1;
grads.W{N-1}=-alpha*mean(a3');
% output constants
grads.b{N-1}=-alpha*mean(deltas{N}');
grads.b{N-1} = [grads.b{N-1},grads.b{N-1}]';
%size(grads.b{N-1})

end
