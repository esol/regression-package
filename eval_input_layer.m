function out = eval_input_layer(in)
% input evaluation
% output is the input repeated once

% this is a cosmetic speedup
%out=[in;in]/1e3;
out=[in;-in]/1;

end
