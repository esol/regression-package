function net2 = single_update(net,input,target)
% update with one time series
% net2 is updated network
% net is old network
% input is input sequence
% target is the target sequence
%alpha=0.0000088835;
alpha=1e-1;

% calculate output of the network
[out,aux] = eval_network(net,input);
% calculate deltas
deltas = calculate_deltas(target,aux,net);
deltas;
% calculate gradients
grads = stochastic_gradients(deltas,aux,alpha,net);

% result
net2=update_weights(net,grads);

end
