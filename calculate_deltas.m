function deltas = calculate_deltas(target,aux,network)
% target is the desired output
% aux is the struct of layer outputs
% network is the network struct
N = network.N;
sizes= network.sizes;
t = size(aux{N},2);
% output is linear
deltaout = -target+aux{N};
% overvalue the first value
%deltaout(1,:) = 1e1*deltaout(1,:);

% first delta
deltas{N} = deltaout;
%deltas{N-1} = aux{N-1}.*(1-aux{N-1});
% go through hidden layers
for i= (N-1):-1:2
%i
    deltas{i} = zeros(2*network.layer{i-1}.nout,t);
    % derivatives at different times
%    der=aux{i}.*(1.-aux{i});

%for sigmoid [0,1] 
%der=aux{i}-aux{i}.*aux{i};
% for sigmoid [-1,1]
der=0.5*(1-aux{i}.*aux{i});

    M = size(der,1);
% go through j
    painomatriisi = network.layer{i}.W;
%    for j=1:M
%        for k=1:size(deltas{i+1},1)
    for j=1:size(painomatriisi,2)


        for k=1:size(painomatriisi,1)
	tmp=deltas{i+1}(k,:)*painomatriisi(k,j);
%	tmp2=der(j,:).*tmp;
	size(der(j,:));
tmp3=der(j,:);
%size(tmp3)
%size(tmp)
%size(tmp3.*tmp)
tmp2=tmp3.*tmp;
size(deltas{i}(j,:));
size(tmp2);
	deltas{i}(j,:) = deltas{i}(j,:)+tmp2;
        end		
    end	    

end	
% input deltas
deltas{1} = zeros(2*network.layer{1}.nin,t);
deltas{1};
    der=aux{1}.*(1.-aux{1});
    M = size(der,1);
    painomatriisi = network.layer{1}.W;
    for j=1:M
        for k=1:size(deltas{2},1)/2
        tmp=deltas{2}(k,:)*painomatriisi(k,j);
	deltas{1}(j,:) = deltas{1}(j,:)+tmp;
        end		
    end	    



end
