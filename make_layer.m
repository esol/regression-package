function layer = make_layer(Nout, Nin)
% hidden layer of a bidirectional recurrent nn
% input: Nout, Nin
% hidden=make_layer(Nout,Nin);
layer.nout = Nout;
layer.nin = Nin;

% rand gives numbers from (0,1)
layer.W = rand(Nout, 2*Nin)*2-1;
%backwards matrix
layer.B = rand(Nout,Nout)*2-1;
%forwards matrix
layer.F = rand(Nout,Nout)*2-1;
% biases
layer.b = rand(2*Nout,1)*2-1;
% output
layer.out = zeros(2*Nout,1);


end
