function out = eval_hidden_layer(layer, input)
% compute output of a hidden layer
% layer is layer struct, input is the output of previous layer
% size(input) == 2*Nout(-1) , t

% work contains the input to each neuron at all times
work = layer.W * input;
t = size(input,2);

% number of neurons
N =layer.nout;
% setting output to 0
out = [work;work]*0;

% forward out
%out(1:N,1) = work(:,1);
%out(1:N,1) = out(1:N,1) + layer.b(1:N);
%out(1:N,1) = sigmoid(out(1:N,1));
out(1:N,1) = sigmoid(work(:,1)+layer.b(1:N));

for i=2:t 
%    out(1:N,i) = work(1:N,i);
%    out(1:N,i) = out(1:N,i)+layer.b(1:N)+ layer.F*out(1:N,i-1);
%    out(1:N,i) = sigmoid(out(1:N,i));    
%out(1:N,i) = sigmoid(work(1:N,i),i+layer.b(1:N)+layer.F*out(1:N,i-1));
out(1:N,i) = sigmoid(work(1:N,i)+layer.b(1:N)+layer.F*out(1:N,i-1));
end	

% backward out
%out(N+1:2*N,1) = work(:,1);
%out(N+1:2*N,1) = out(N+1:2*N,1) + layer.b(N+1:2*N);
%out(N+1:2*N,1) = sigmoid(out(N+1:2*N,1));
out(N+1:2*N,1) = sigmoid(work(:,1) + layer.b(N+1:2*N));
for i=2:t 
%    out(N+1:2*N,i) = work(1:N,i);
%    out(N+1:2*N,i) = out(N+1:2*N,i)+layer.b(N+1:2*N)+ layer.F*out(N+1:2*N,i-1);
%    out(N+1:2*N,i) = sigmoid(out(N+1:2*N,i));    
out(N+1:2*N,i) = sigmoid(work(1:N,i)+layer.b(N+1:2*N)+layer.B*out(N+1:2*N,i-1));
end	


end

