function [MSE,error,SSE,MAE,SAE] = calculate_error(net,input,target)
% calculates different errors
% net is network
% input is input
% target is target
out = eval_network(net,input);
error = target -out;
SSE = sum(sum(error.*error));
t = size(error,2);
MSE = SSE/t;
SAE = sum(sum(abs(error)));
MAE = SAE/t;

end
