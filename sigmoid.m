function res = sigmoid(x)

%res = 1./(1+e.^(-x));
%res = (e.^x-1)./(e.^x+1);
res = (exp(x)-1)./(exp(x)+1);
%this might help
res = min(res,1);

end
