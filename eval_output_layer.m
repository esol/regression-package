function out = eval_output_layer(layer, input)
% compute output of an output layer
% layer is layer struct, input is the output of previous layer
% size(input) == 2*Nout(-1) , t

out = layer.W * input;
out = [out;out];
t = size(input,2);
N = size(layer.W,1);
qq=mean(layer.b')';
for i=1:t
%    out(:,i) = out(:,i)+layer.b;
    out(:,i) = out(:,i)+qq;
%    out(:,i) = out(:,i);
end

out = out(1:N,:);

% comment out if needed
%out(1,:) = round(out(1,:));

end

