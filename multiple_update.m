function net2 = multiple_update(net,data)
% net2 is the new net
% net is the old net
% data is the struct containing input output pairs
net2=net;

for i=1:data.N
% data.N is the number of pairs in data
    net2 = single_update(net2, data.in{i}, data.out{i});
end	


end
