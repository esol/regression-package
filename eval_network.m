function [result, aux] = eval_network(network,input)
% evaluates a bidirectional recurrent network
% network is the network, input is the matrix of inputs

aux{network.N}=[];

% size of time steps
t= size(input,2);
result = zeros(network.sizes(network.N),t);
% aux is a struct of intermediate evaluations
aux{1} = eval_input_layer(input);

% go through hidden layers
for i=2:network.N-1
    aux{i} = eval_hidden_layer(network.layer{i-1},aux{i-1});
end	
aux{network.N} = eval_output_layer(network.layer{network.N-1},aux{network.N-1});
result = aux{network.N};

end
