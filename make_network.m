function network = make_network(sizes)
% constructor for nn
% sizes is vector of integers
% first number is size of input
% last number is size of output
% middle ones are the sizes of hidden layers

network.N = numel(sizes);
network.sizes = sizes;

for i= 2:network.N
    sven = make_layer(sizes(i),sizes(i-1));
    network.layer{i-1} = sven;
end	



end
