function mse = set_mse(net, data)
% mse is the average mean square error of the predictions on data
% net is net
% data is a struct with data.N, data.in{:}, data.out{:}
mse = 0;
for i = 1:data.N
    mse = mse + calculate_error(net,data.in{i},data.out{i});
end	
mse = mse/data.N;

end
